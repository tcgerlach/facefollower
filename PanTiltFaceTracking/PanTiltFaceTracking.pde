/**********************************************************************************************
* Author: Thomas C. Gerlach
* Notes: 
* 1) This uses the first camera on your computer. This will typically be your webcam. Since 
*    I do not know Processing or OpenCV well enough yet, I simply disabled that camera on 
*    my laptop so that this application uses the secondary camera located on the pan/tilt
*    assembly. 
* 2) The pan/tilt assembly is powered by a separate 2 Amp USB power supply fed to the servos
*    by a USB breakout board. 
* 3) This program assumes you are using the first serial port on your computer. Given
*    that nobody uses many serial devices anymore, this is probably reasonable for most 
*    machines. If you need to use a different one, you will need to update the script
* 4) If everything moves backwards, you have the camera upside down.
*
* Based on code by Ryan Owens for SparkFun Electronics
* Uses the OpenCV real-time computer vision  framework from Intel
* Based on the OpenCV Processing Examples from ubaa.net
*
* The Pan/Tilt Face Tracking Sketch interfaces with an Arduino Main board to control
* two servos, pan and tilt, which are connected to a webcam. The OpenCV library
* looks for a face in the image from the webcam. If a face is detected the sketch
* uses the coordinates of the face to manipulate the pan and tilt servos to move the webcam
* in order to keep the face in the center of the frame.
*
* Setup-
* A webcam must be connected to the computer.
* An Arduino must be connected to the computer. Note the port which the Arduino is connected on.
* The Arduino must be loaded with the SerialServoControl Sketch.
* Two servos mounted on a pan/tilt backet must be connected to the Arduino pins 2 and 3.
* The Arduino must be powered by an external power supply.
**********************************************************************************************/
import gab.opencv.*;
import java.awt.Rectangle;
import processing.serial.*;
import processing.video.*;

// Screen Size Parameters
final int width = 640;
final int height = 480;
final int scale = 2;

// The pan/tilt servo ids for the Arduino serial command interface.
final char tiltChannel = 0;
final char panChannel = 1;

//The variables correspond to the middle of the screen, and will be compared to the midFace values
final int midScreenY = (height/2/scale);
final int midScreenX = (width/2/scale);
final int error = 10;  //This is the acceptable 'error' for the center of the screen. 

// The degree of change that will be applied to the servo each time we update the position.
int stepSize=1;

// application resources
OpenCV opencv;
Capture video;
Serial port;

// Variables for keeping track of the current servo positions.
char servoTiltPosition = 90;
char servoPanPosition = 90;

void setup() {
  //Create a window for the sketch.
  size(640, 480);

  // create OpenCV instance, video, etc
  video = new Capture(this, width/2, height/2);
  opencv = new OpenCV(this, width/2, height/2);
  opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
  video.start();

  //select first com-port from the list (change the number in the [] if your sketch fails to connect to the Arduino)
  port = new Serial(this, Serial.list()[0], 57600);   //Baud rate is set to 57600 to match the Arduino baud rate.

  //Send the initial pan/tilt angles to the Arduino to set the device up to look straight forward.
  port.write(tiltChannel);        //Send the Tilt Servo ID
  port.write(servoTiltPosition);  //Send the Tilt Position (currently 90 degrees)
  port.write(panChannel);         //Send the Pan Servo ID
  port.write(servoPanPosition);   //Send the Pan Position (currently 90 degrees)
}

void draw() {
  // show video
  scale(scale);
  opencv.loadImage(video);
  image(video, 0, 0);
  
  // setup face marker
  noFill();
  stroke(0,255,0);
  strokeWeight(3);

  // draw face area(s)
  Rectangle[] faces = opencv.detect();
  for(int i=0; i < faces.length; i++) {
    rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height);
  }
    
  // Find out if any faces were detected.
  if(faces.length > 0) {            
    // If a face was found, find the midpoint of the first face in the frame.
    int midFaceY = faces[0].y + (faces[0].height / 2);
    int midFaceX = faces[0].x + (faces[0].width / 2);
    
    // Find out if the Y component of the face is above or below the middle of the screen.
    if(midFaceY < (midScreenY - error)) { //<>//
      if(servoTiltPosition >= 5) servoTiltPosition -= stepSize;
    } else if(midFaceY > (midScreenY + error)) {
      if(servoTiltPosition <= 175) servoTiltPosition +=stepSize;
    }
    
    // Find out if the X component of the face is to the left or right of the middle of the screen.
    if(midFaceX > (midScreenX - error)) {
      if(servoPanPosition >= 5) servoPanPosition -= stepSize;
    } else if(midFaceX < (midScreenX + error)){
      if(servoPanPosition <= 175)servoPanPosition +=stepSize;
    }
  }
  
  //Update the servo positions by sending the serial command to the Arduino.
  port.write(tiltChannel);       //Send the tilt servo ID
  port.write(servoTiltPosition); //Send the updated tilt position.
  port.write(panChannel);        //Send the Pan servo ID
  port.write(servoPanPosition);  //Send the updated pan position. 
}

void captureEvent(Capture c) {
  c.read();
}