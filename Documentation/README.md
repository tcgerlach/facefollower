# Face Follower 

This project is based on [HackerBoxes 24](http://www.instructables.com/id/HackerBox-0024-Vision-Quest/) 
with code based on an [old project from SparkFun](https://www.sparkfun.com/tutorials/304). 

The Arduino is connected to the pan/tilt servos.  
![Arduino setup](setup-arduino.jpg)  
The pan/tilt mechanism is setup to hold the camera.  
![Pan/Tilt Setup](setup-pan-tilt.jpg)  
And both the Arduino and the camera are connected to the host computer  
![Project Setup](setup-project.jpg)  

Here are photos of the completed project.  
![Arduino](photo-arduino.jpg) ![Pan/Tilt](photo-pan-tilt.jpg) 