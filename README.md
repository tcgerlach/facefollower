# Face Follower

This project includes Arduino code to control a pan/tilt setup for a camera as well 
as an OpenCV app to follow a face that appears in front of the camera.

See the [Documentation](Documentation/README.md) for more information.